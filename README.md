# Job4You

Projet pour la bourse coddity

## Installation

Commencez par installer les dépendances :

```
composer install
```

Après avoir configuré la base de donnée dans le fichier *.env*, lancez 
```
php bin/console doctrine:database:create
```
pour créer la base de donnée, puis
```
php bin/console doctrine:schema:create
```
pour créer le schéma de la base de données.

La commande suivante permet de lancer un serveur de developpement
```
php bin/console server:start
```

## Remarques

Pour que le site fonctionne correctement, il faut remplir les tables *category* et *field*.
N'ayant pas réalisé de script de remplissage pour ces tables, il faut les remplir à la main.
A noter qu'il faut absolument qu'à chaque catégorie soit associé une entité de type *Field*.

Le site est accessible en test à l'adresse :

http://vps522414.ovh.net:12345
