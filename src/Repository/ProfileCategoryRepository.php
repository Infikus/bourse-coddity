<?php

namespace App\Repository;

use App\Entity\Job;
use App\Entity\ProfileCategory;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ProfileCategory|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProfileCategory|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProfileCategory[]    findAll()
 * @method ProfileCategory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProfileCategoryRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ProfileCategory::class);
    }

    /**
     * @param User $user
     * @return ProfileCategory[]
     */
    public function getAllByUser(User $user)
    {
        return $this->createQueryBuilder('pc')
            ->innerJoin('pc.profile', 'p')
            ->innerJoin('p.userInfo', 'ui')
            ->innerJoin('ui.user', 'u')
            ->andWhere('u.id = :userId')
            ->setParameter('userId', $user->getId())
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param Job $job
     * @return ProfileCategory[]
     */
    public function getAllByJob(Job $job)
    {
        return $this->createQueryBuilder('pc')
            ->innerJoin('pc.profile', 'p')
            ->innerJoin('p.job', 'j')
            ->andWhere('j.id = :jobId')
            ->setParameter('jobId', $job->getId())
            ->getQuery()
            ->getResult()
            ;
    }
}
