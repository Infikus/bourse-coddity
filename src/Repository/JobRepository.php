<?php

namespace App\Repository;

use App\Entity\Job;
use App\Entity\Society;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\NonUniqueResultException;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Job|null find($id, $lockMode = null, $lockVersion = null)
 * @method Job|null findOneBy(array $criteria, array $orderBy = null)
 * @method Job[]    findAll()
 * @method Job[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JobRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Job::class);
    }

    /**
     * @param int $id
     * @return Job|null
     */
    public function get(int $id) {
        try {
            return $this->createQueryBuilder('j')
                ->innerJoin('j.society', 's')
                ->addSelect('s')
                ->leftJoin('j.bestProfile', 'p')
                ->leftJoin('p.profileCategories', 'pc')
                ->leftJoin('pc.category', 'c')
                ->leftJoin('p.profileFields', 'pf')
                ->leftJoin('pf.field', 'f')
                ->addSelect('p, pc, pf, f, c')
                ->andWhere('j.id = :jobId')
                ->setParameter('jobId', $id)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $exception) {
            return null;
        }
    }

    /**
     * @param Society $society
     * @return Pagerfanta
     */
    public function getSocietyJobs(Society $society) {
        $qb = $this->createQueryBuilder('j')
            ->innerJoin('j.society', 's')
            ->leftJoin('j.applications', 'a')
            ->andWhere('s.id = :societyId')
            ->orderBy('j.limit_date', 'DESC')
            ->setParameter('societyId', $society->getId())
        ;
        $adapter = new DoctrineORMAdapter($qb);
        return new Pagerfanta($adapter);
    }

    /**
     * @return Pagerfanta
     */
    public function search() {
        $qb = $this->createQueryBuilder('j')
            ->innerJoin('j.society', 's')
            ->orderBy('j.created_at', 'DESC')
        ;
        $adapter = new DoctrineORMAdapter($qb);
        return new Pagerfanta($adapter);
    }
}
