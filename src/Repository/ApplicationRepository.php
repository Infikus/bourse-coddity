<?php

namespace App\Repository;

use App\Entity\Application;
use App\Entity\Society;
use App\Entity\User;
use App\Form\ApplicationType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Application|null find($id, $lockMode = null, $lockVersion = null)
 * @method Application|null findOneBy(array $criteria, array $orderBy = null)
 * @method Application[]    findAll()
 * @method Application[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ApplicationRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Application::class);
    }

    /**
     * @param Society $society
     * @return Pagerfanta
     */
    public function getAllBySociety(Society $society) {
        $qb = $this->createQueryBuilder('a')
            ->innerJoin('a.job', 'j')
            ->innerJoin('j.society', 's')
            ->andWhere('s.id = :societyId')
            ->setParameter('societyId', $society->getId())
        ;
        $adapter = new DoctrineORMAdapter($qb);
        return new Pagerfanta($adapter);
    }

    /**
     * @param User $user
     * @return Pagerfanta
     */
    public function getAllByUser(User $user) {
        $qb = $this->createQueryBuilder('a')
            ->innerJoin('a.job', 'j')
            ->innerJoin('j.society', 's')
            ->innerJoin('a.userInfo', 'ui')
            ->innerJoin('ui.user', 'u')
            ->andWhere('u.id = :userId')
            ->setParameter('userId', $user->getId())
        ;
        $adapter = new DoctrineORMAdapter($qb);
        return new Pagerfanta($adapter);
    }

    /**
     * @param int $id
     * @return Application|null
     */
    public function get(int $id) {
        try {
            return $this->createQueryBuilder('a')
                ->leftJoin('a.job', 'j')
                ->leftJoin('j.society', 's')
                ->leftJoin('a.userInfo', 'ui')
                ->leftJoin('ui.user', 'u')
		->leftJoin('ui.cvs', 'cv')
		->addSelect('j, s, ui, u, cv')
                ->andWhere('a.id = :id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getOneOrNullResult()
            ;
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

//    /**
//     * @return Application[] Returns an array of Application objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Application
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
