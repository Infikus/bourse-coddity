<?php

namespace App\Repository;

use App\Entity\ProfileField;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ProfileField|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProfileField|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProfileField[]    findAll()
 * @method ProfileField[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProfileFieldRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ProfileField::class);
    }

//    /**
//     * @return ProfileField[] Returns an array of ProfileField objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProfileField
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
