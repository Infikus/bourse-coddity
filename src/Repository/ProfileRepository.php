<?php

namespace App\Repository;

use App\Entity\Category;
use App\Entity\Job;
use App\Entity\Profile;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Profile|null find($id, $lockMode = null, $lockVersion = null)
 * @method Profile|null findOneBy(array $criteria, array $orderBy = null)
 * @method Profile[]    findAll()
 * @method Profile[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProfileRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Profile::class);
    }

    /**
     * @param User $user
     * @return Profile|null
     */
    public function getByUser(User $user) {
        try {
            return $this->createQueryBuilder('p')
                ->leftJoin('p.profileCategories', 'pc')
                ->leftJoin('p.profileFields', 'pf')
                ->leftJoin('p.userInfo', 'ui')
                ->leftJoin('ui.user', 'u')
                ->andWhere('u.id = :userId')
                ->setParameter('userId', $user->getId())
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @param User $user
     * @param Category $category
     * @return Profile|null
     */
    public function getByUserAndCategory(User $user, Category $category) {
        try {
            return $this->createQueryBuilder('p')
                ->leftJoin('p.profileCategories', 'pc')
                ->leftJoin('p.profileFields', 'pf')
                ->leftJoin('pf.field', 'f')
                ->leftJoin('f.category', 'fc')
                ->leftJoin('p.userInfo', 'ui')
                ->leftJoin('ui.user', 'u')
                ->addSelect('pc, pf, f, fc, ui, u')
                ->andWhere('u.id = :userId')
                ->setParameter('userId', $user->getId())
                ->andWhere('fc.id is null or fc.id = :fcId')
                ->setParameter('fcId', $category->getId())
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @param Job $job
     * @return Profile|null
     */
    public function getByJob(Job $job) {
        try {
            return $this->createQueryBuilder('p')
                ->leftJoin('p.profileCategories', 'pc')
                ->leftJoin('p.profileFields', 'pf')
                ->leftJoin('p.job', 'j')
                ->andWhere('j.id = :jobId')
                ->setParameter('jobId', $job->getId())
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @param Job $job
     * @param Category $category
     * @return Profile|null
     */
    public function getByJobAndCategory(Job $job, Category $category) {
        try {
            return $this->createQueryBuilder('p')
                ->leftJoin('p.profileCategories', 'pc')
                ->leftJoin('p.profileFields', 'pf')
                ->leftJoin('pf.field', 'f')
                ->leftJoin('f.category', 'fc')
                ->leftJoin('p.job', 'j')
                ->addSelect('pc, pf, f, fc, j')
                ->andWhere('j.id = :jobId')
                ->setParameter('jobId', $job->getId())
                ->andWhere('fc.id is null or fc.id = :fcId')
                ->setParameter('fcId', $category->getId())
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}
