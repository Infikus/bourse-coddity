<?php

namespace App\Form;

use App\Entity\Field;
use App\Entity\ProfileField;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProfileFieldType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('value', RangeType::class, [
                'attr' => [
                    'min' => 1,
                    'max' => 10,
                ],
            ])
            ->add('experience', IntegerType::class, [
                'attr' => [
                    'min' => 0,
                ],
            ])
            ->add('field', EntityType::class, [
                'class' => Field::class,
                'choice_label' => 'name',
            ]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProfileField::class,
        ]);
    }
}
