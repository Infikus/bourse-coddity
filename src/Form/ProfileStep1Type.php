<?php

namespace App\Form;

use App\Entity\Job;
use App\Entity\Profile;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProfileStep1Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('request', ChoiceType::class, [
                'choices' => Job::getTypeChoices(),
            ])
            ->add('profileCategories', CollectionType::class, [
                "entry_type" => ProfileCategoryType::class,
                "allow_delete" => true,
                "allow_add" => true,
                "prototype" => true,
                "prototype_name" => "profile_categories_name",
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Profile::class,
        ]);
    }
}
