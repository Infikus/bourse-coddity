<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CVRepository")
 * @Vich\Uploadable()
 */
class CV implements \Serializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lang;

    /**
     * @var File
     *
     * @Vich\UploadableField(mapping="cv_file", fileNameProperty="cvName", originalName="cvOriginalName", size="cvSize")
     */
    private $cvFile;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $cvName;

    /**
     * @ORM\Column(type="integer")
     *
     * @var integer
     */
    private $cvSize;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $cvOriginalName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @var UserInfo
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\UserInfo", inversedBy="cvs")
     */
    private $userInfo;

    public function __construct()
    {
        $this->lang = 'fr';
        $this->updatedAt = new \DateTimeImmutable();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getLang(): ?string
    {
        return $this->lang;
    }

    public function setLang(?string $lang): self
    {
        $this->lang = $lang;

        return $this;
    }

    public function getCvName(): ?string
    {
        return $this->cvName;
    }

    public function setCvName(?string $cvName): self
    {
        $this->cvName = $cvName;

        return $this;
    }

    public function getCvFile(): ?File
    {
        return $this->cvFile;
    }

    /**
     * @param File|UploadedFile $cvFile
     * @return CV
     */
    public function setCvFile(?File $cvFile = null): self
    {
        $this->cvFile = $cvFile;

        if (null !== $cvFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt(\DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    public function getCvSize(): ?int
    {
        return $this->cvSize;
    }

    public function setCvSize(?int $size): self
    {
        $this->cvSize = $size;

        return $this;
    }

    /**
     * @return UserInfo
     */
    public function getUserInfo(): UserInfo
    {
        return $this->userInfo;
    }

    /**
     * @param UserInfo $userInfo
     */
    public function setUserInfo(UserInfo $userInfo): void
    {
        $this->userInfo = $userInfo;
    }

    public function getCvOriginalName(): ?string
    {
        return $this->cvOriginalName;
    }

    public function setCvOriginalName(?string $name): self
    {
        $this->cvOriginalName = $name;

        return $this;
    }

    /**
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize()
    {
        // TODO: Implement serialize() method.
    }

    /**
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
        // TODO: Implement unserialize() method.
    }
}
