<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\JobRepository")
 */
class Job
{
    const TYPE = [
        "Emploi plein temps",
        "Emploi mi temps",
        "Stage",
        "Travail saisonnier",
    ];

    const SHORT_TYPE = [
        "Plein temps",
        "Mi temps",
        "Stage",
        "Saisonnier",
    ];

    public static function getTypeChoices() {
        $choices = [];
        foreach (self::TYPE as $index => $val) {
            $choices[$val] = $index;
        }
        return $choices;
    }
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     */
    private $type;

    /**
     * @ORM\Column(type="datetime")
     */
    private $limit_date;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @var Profile
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Profile", inversedBy="job", cascade={"persist", "remove"})
     */
    private $bestProfile;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Application", mappedBy="job", cascade={"remove"})
     */
    private $applications;

    public function __construct()
    {
        $this->created_at = new \DateTime('now');
        $this->limit_date = new \DateTime('now');
        $this->applications = new ArrayCollection();
    }

    /**
     * @var Society
     *
     * @ORM\ManyToOne(targetEntity="Society", inversedBy="jobs")
     */
    private $society;

    public function getId()
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getLimitDate(): ?\DateTimeInterface
    {
        return $this->limit_date;
    }

    public function setLimitDate(\DateTimeInterface $limit_date): self
    {
        $this->limit_date = $limit_date;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreateAt(): \DateTime
    {
        return $this->created_at;
    }

    /**
     * @return Society|null
     */
    public function getSociety(): ?Society
    {
        return $this->society;
    }

    /**
     * @param Society $society
     * @return Job
     */
    public function setSociety(Society $society): self
    {
        $this->society = $society;

        return $this;
    }

    /**
     * @return Profile|null
     */
    public function getBestProfile(): ?Profile
    {
        return $this->bestProfile;
    }

    /**
     * @param Profile $profile
     * @return Job
     */
    public function setBestProfile(Profile $profile): self
    {
        $this->bestProfile = $profile;

        return $this;
    }

    public function getApplications(): Collection
    {
        return $this->applications;
    }

    public function addApplication(Application $application): self
    {
        $this->applications->add($application);

        return $this;
    }

    public function removeApplication(Application $application): self
    {
        $this->applications->removeElement($application);

        return $this;
    }
}
