<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserInfoRepository")
 */
class UserInfo
{
    const STATUS = [
        "Etudiant",
        "Salarié",
        "Stagiaire",
    ];

    public static function getStatusChoices() {
        $choices = [];
        foreach (self::STATUS as $index => $val) {
            $choices[$val] = $index;
        }
        return $choices;
    }

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @Assert\NotBlank(message="Prénom requis")
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @Assert\NotBlank(message="Nom requis")
     */
    private $lastName;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @Assert\NotBlank(message="Veuillez vous présenter en quelques lignes")
     */
    private $description;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $status;

    /**
     * @var Profile
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Profile", inversedBy="userInfo", cascade={"persist", "remove"})
     */
    private $profile;

    /**
     * @var User
     *
     * @ORM\OneToOne(targetEntity="App\Entity\User", mappedBy="userInfo")
     */
    private $user;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Application", mappedBy="userInfo", cascade={"persist", "remove"})
     */
    private $applications;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\CV", mappedBy="userInfo", cascade={"persist", "remove"})
     */
    private $cvs;

    public function __construct()
    {
        $this->applications = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getProfile(): ?Profile
    {
        return $this->profile;
    }

    public function setProfile(Profile $profile): self
    {
        $this->profile = $profile;

        return $this;
    }

    public function getApplications(): Collection
    {
        return $this->applications;
    }

    public function addApplication(Application $application): self
    {
        $this->applications->add($application);

        return $this;
    }

    public function removeApplication(Application $application): self
    {
        $this->applications->removeElement($application);

        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCvs(): Collection
    {
        return $this->cvs;
    }

    public function addCv(CV $cv): self
    {
        $this->cvs->add($cv);

        return $this;
    }

    public function removeCv(CV $cv): self
    {
        $this->cvs->removeElement($cv);

        return $this;
    }

}
