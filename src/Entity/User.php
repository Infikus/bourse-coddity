<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(fields="email", message="Email déjà utilisé")
 */
class User implements UserInterface
{
    const RECRUITER_TYPE = 0;
    const CANDIDATE_TYPE = 1;

    const TYPES = [
        self::RECRUITER_TYPE => 'Recuteur',
        self::CANDIDATE_TYPE => 'Candidat',
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\NotBlank()
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(type="text")
     */
    private $roles;

    /**
     * @var Society
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Society", inversedBy="user", cascade={"persist", "remove"})
     */
    private $society; // If he is a recruiter

    /**
     * @var UserInfo
     *
     * @ORM\OneToOne(targetEntity="App\Entity\UserInfo", inversedBy="user", cascade={"persist", "remove"})
     */
    private $userInfo; // If he is a candidate

    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(min="0", max="1", minMessage="Valeur incorrecte", maxMessage="Valeur incorrecte", invalidMessage="Valeur incorrecte")
     */
    private $type;

    public function __construct()
    {
        $this->roles = serialize([]);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getRoles(): array
    {
        return unserialize($this->roles);
    }

    public function addRole($role): self
    {
        $roles = unserialize($this->roles);
        $roles[] = $role;
        $this->roles = serialize(array_unique($roles));

        return $this;
    }

    public function removeRole($role): self
    {
        $roles = unserialize($this->roles);
        $roles = array_diff($roles, [$role]);
        $this->roles = serialize($roles);

        return $this;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername(): string
    {
        return $this->getEmail();
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials(): self
    {
        return $this;
    }

    /**
     * @return Society
     */
    public function getSociety(): ?Society
    {
        return $this->society;
    }

    /**
     * @param Society $society
     * @return User
     */
    public function setSociety(Society $society): self
    {
        $this->society = $society;
        return $this;
    }

    /**
     * @return UserInfo
     */
    public function getUserInfo(): ?UserInfo
    {
        return $this->userInfo;
    }

    /**
     * @param UserInfo $userInfo
     * @return User
     */
    public function setUserInfo(UserInfo $userInfo): self
    {
        $this->userInfo = $userInfo;
        return $this;
    }

    public function isCandidate(): bool
    {
        return $this->userInfo !== null;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;
        return $this;
    }

    public function isValid() {
        return ($this->society !== null && $this->society->getName()) ||
            ($this->userInfo !== null && $this->userInfo->getFirstName());
    }
}
