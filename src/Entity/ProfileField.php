<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProfileFieldRepository")
 */
class ProfileField
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $value;

    /**
     * @ORM\Column(type="integer")
     *
     * @Assert\GreaterThanOrEqual(value="0", message="Doit être supérieur à 0")
     */
    private $experience;

    /**
     * @var Profile
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Profile", inversedBy="profileFields", cascade={"persist"})
     */
    private $profile;

    /**
     * @var Field
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Field")
     */
    private $field;

    public function getId()
    {
        return $this->id;
    }

    public function getValue(): ?int
    {
        return $this->value;
    }

    public function setValue(int $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getExperience(): ?int
    {
        return $this->experience;
    }

    /**
     * @param int $experience
     * @return ProfileField
     */
    public function setExperience(int $experience): self
    {
        $this->experience = $experience;

        return $this;
    }

    /**
     * @return Profile|null
     */
    public function getProfile(): ?Profile
    {
        return $this->profile;
    }

    /**
     * @param Profile $profile
     * @return ProfileField
     */
    public function setProfile(Profile $profile): self
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * @return Field|null
     */
    public function getField(): ?Field
    {
        return $this->field;
    }

    /**
     * @param Field $field
     * @return ProfileField
     */
    public function setField(Field $field): self
    {
        $this->field = $field;

        return $this;
    }
}
