<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ApplicationRepository")
 */
class Application
{
    const STATUS_WAITING = 0;
    const STATUS_ACCEPTED = 1;
    const STATUS_REJECTED = 2;

    const STATUS = [
        self::STATUS_WAITING => "En attente",
        self::STATUS_ACCEPTED => "Acceptée",
        self::STATUS_REJECTED => "Rejetée",
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="text")
     */
    private $text;

    /**
     * @ORM\Column(type="integer")
     *
     * @var integer
     */
    private $status;

    /**
     * @var Job
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Job", inversedBy="applications")
     * @ORM\JoinColumn(nullable=false)
     */
    private $job;

    /**
     * @var UserInfo
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\UserInfo", inversedBy="applications")
     * @ORM\JoinColumn(nullable=false)
     */
    private $userInfo;

    public function __construct()
    {
        $this->created_at = new \DateTime('now');
        $this->status = self::STATUS_WAITING;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;
        return $this;
    }

    public function getStatusString(): string
    {
        return self::STATUS[$this->status];
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getJob(): Job
    {
        return $this->job;
    }

    public function setJob(Job $job): self
    {
        $this->job = $job;

        return $this;
    }

    public function getUserInfo(): UserInfo
    {
        return $this->userInfo;
    }

    public function setUserInfo(UserInfo $userInfo): self
    {
        $this->userInfo = $userInfo;

        return $this;
    }
}
