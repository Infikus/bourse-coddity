<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProfileCategoryRepository")
 */
class ProfileCategory
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     *
     * @Assert\Range(min="1", max="10", minMessage="Doit être compris entre 1 et 10", maxMessage="Doit être compris entre 1 et 10")
     */
    private $weight;

    /**
     * @ORM\Column(type="integer")
     *
     * @Assert\GreaterThanOrEqual(value="0", message="Doit être supérieur à 0")
     */
    private $experience;

    /**
     * @var Profile
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Profile", inversedBy="profileCategories", cascade={"persist"})
     */
    private $profile;

    /**
     * @var Category
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Category")
     */
    private $category;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getWeight(): ?int
    {
        return $this->weight;
    }

    /**
     * @param int $weight
     * @return ProfileCategory
     */
    public function setWeight(int $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getExperience(): ?int
    {
        return $this->experience;
    }

    /**
     * @param int $experience
     * @return ProfileCategory
     */
    public function setExperience(int $experience): self
    {
        $this->experience = $experience;

        return $this;
    }

    /**
     * @return Profile|null
     */
    public function getProfile(): ?Profile
    {
        return $this->profile;
    }

    public function setProfile(Profile $profile): self
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * @return Category|null
     */
    public function getCategory(): ?Category
    {
        return $this->category;
    }

    /**
     * @param Category $category
     * @return ProfileCategory
     */
    public function setCategory(Category $category): self
    {
        $this->category = $category;

        return $this;
    }
}
