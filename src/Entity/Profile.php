<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProfileRepository")
 */
class Profile
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $request;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\ProfileCategory", mappedBy="profile", cascade={"persist", "remove"})
     */
    private $profileCategories;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\ProfileField", mappedBy="profile", cascade={"persist", "remove"})
     */
    private $profileFields;

    /**
     * @var UserInfo|null
     *
     * @ORM\OneToOne(targetEntity="App\Entity\UserInfo", mappedBy="profile", cascade={"persist"})
     */
    private $userInfo;

    /**
     * @var Job|null
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Job", mappedBy="bestProfile", cascade={"persist"})
     */
    private $job;

    public function __construct()
    {
        $this->profileFields = new ArrayCollection();
        $this->profileCategories = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }


    public function getRequest(): ?int
    {
        return $this->request;
    }

    public function setRequest(int $request): self
    {
        $this->request = $request;

        return $this;
    }

    /**
     * @return Collection<ProfileCategory>
     */
    public function getProfileCategories(): Collection
    {
        return $this->profileCategories;
    }

    /**
     * @param ProfileCategory $profileCategory
     * @return Profile
     */
    public function addProfileCategory(ProfileCategory $profileCategory): self
    {
        $this->profileCategories->add($profileCategory);

        return $this;
    }

    /**
     * @param ProfileCategory $profileProfileCategory
     * @return Profile
     */
    public function removeProfileCategory(ProfileCategory $profileProfileCategory): self
    {
        $this->profileCategories->removeElement($profileProfileCategory);

        return $this;
    }

    /**
     * @return Collection
     */
    public function getProfileFields(): Collection
    {
        return $this->profileFields;
    }

    /**
     * @param ProfileField $field
     * @return Profile
     */
    public function addProfileField(ProfileField $field): self
    {
        $this->profileFields->add($field);

        return $this;
    }

    /**
     * @param ProfileField $field
     * @return Profile
     */
    public function removeProfileField(ProfileField $field): self
    {
        $this->profileFields->removeElement($field);

        return $this;
    }

    public function resetProfileFields(): self
    {
        $this->profileFields = new ArrayCollection();

        return $this;
    }

    /**
     * @return UserInfo|null
     */
    public function getUserInfo(): ?UserInfo
    {
        return $this->userInfo;
    }

    /**
     * @param UserInfo $userInfo
     * @return Profile
     */
    public function setUserInfo(UserInfo $userInfo): self
    {
        $this->userInfo = $userInfo;

        return $this;
    }

    /**
     * @return Job|null
     */
    public function getJob(): ?Job
    {
        return $this->job;
    }

    /**
     * @param Job $job
     * @return Profile
     */
    public function setJob(Job $job): self
    {
        $this->job = $job;

        return $this;
    }

    public function getDetailsArray(): array
    {
        $res = [];
        if ($this->profileCategories == null) return $res;

        // On parcours toutes les catégories pour initialiser le tableau de resultat
        foreach ($this->profileCategories as $profileCategory) {
            $cat = $profileCategory->getCategory();
            $res[$cat->getId()] = [
                'weight' => $profileCategory->getWeight(),
                'fields' => [],
            ];
        }

        // Puis on regarde tous les Fields pour compléter le tableau
        foreach ($this->profileFields as $profileField) {
            $catId = $profileField->getField()->getCategory()->getId();
            if (!array_key_exists($catId, $res)) continue;
            $field = $profileField->getField();
            $fieldValue = $profileField->getExperience()*$profileField->getValue();
            if (!array_key_exists($field->getId(), $res[$catId]['fields']))
                $res[$catId]['fields'][$field->getId()] = [];
            if (array_key_exists($field->getGroup(), $res[$catId]['fields'][$field->getId()])) {
                $res[$catId]['fields'][$field->getId()][$field->getGroup()] += $fieldValue;
            } else {
                $res[$catId]['fields'][$field->getId()][$field->getGroup()] = $fieldValue;
            }
        }
        return $res;
    }

    /**
     * Compare deux Profile en se basant sur le tableau généré par getDetailsArray
     * Retourne le score de match (entre 0 et 100)
     */
    public static function calculMatchingScore(array $ref, array $other)
    {
        // Valeur de retour
        $resValue = 0;

        // Valeur max que l'on peut atteindre (défini par la référence)
        $refValue = 0;
        foreach ($ref as $val) {
            $catValue = 0;
            foreach ($val['fields'] as $field)
                foreach($field as $value) $catValue += $value;
            $refValue += $catValue*$val['weight'];
        }

        if ($refValue === 0) return 0; //Ne devrait pas arriver

        // Calcul de resValue
        foreach ($other as $index => $val) {
            $catValue = 0;
            // On vérifie que la catégorie est aussi dans le profil de référence
            if (!array_key_exists($index, $ref)) continue;
            foreach ($val['fields'] as $fIndex => $field) {
                // Même verif mais avec le field
                if (!array_key_exists($fIndex, $ref[$index]['fields'])) continue;
                foreach ($field as $group => $value) {
                    if (!array_key_exists($group, $ref[$index]['fields'][$fIndex])) continue;
                    $catValue += min($value, $ref[$index]['fields'][$fIndex][$group]);
                }
            }
            $resValue += $catValue*$val['weight'];
        }

        return min(100, $resValue*100/$refValue);
    }
}
