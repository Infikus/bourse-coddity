<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\CV;
use App\Entity\Profile;
use App\Entity\ProfileCategory;
use App\Entity\UserInfo;
use App\Form\ProfileStep1Type;
use App\Form\ProfileStep2Type;
use App\Form\UserInfoType;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CandidateController extends Controller
{
    /**
     * @Route("/candidate/profile", name="candidate_edit_profile")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editProfile(Request $request)
    {
        $user = $this->getUser();
        $userInfo = $user->getUserInfo();


        $form = $this->createForm(UserInfoType::class, $userInfo);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dm = $this->getDoctrine()->getManager();

            foreach ($userInfo->getCvs() as $cv) {
                // Check if there is a file
                if (null === $cv->getCvFile()) {
                    if ($cv->getId() !== null && $cv->getLang() === null) {
                        $dm->remove($cv);
                    }
                    $userInfo->removeCv($cv);
                    continue;
                }
                $cv->setUserInfo($userInfo);
            }

            $dm->persist($userInfo);
            $dm->flush();

            $this->addFlash('success', 'Profil mis à jour.');

            return $this->redirectToRoute('candidate_edit_profile');
        }

        $cvs = $userInfo->getCvs();

        return $this->render('candidate/editProfile.html.twig', [
            'form' => $form->createView(),
            'cvs' => $cvs,
        ]);
    }

    /**
     * @Route("/candidate/detailed_profile", name="candidate_edit_detailed_profile")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editDetailedProfileStep1(Request $request)
    {
        $user = $this->getUser();
        $dm = $this->getDoctrine()->getManager();

        // Get all categories
        $categoriesRaw = $dm->getRepository('App:Category')->getAll();
        $categories = [];

        // Set categories to use category's id as an array index
        foreach($categoriesRaw as $category) {
            $categories[$category->getId()] = $category;
        }

        $profile = $dm->getRepository('App:Profile')->getByUser($user) ?: new Profile();
        $profile->setUserInfo($user->getUserInfo());
        $oldCategories = clone $profile->getProfileCategories();

        $selectedCat = [];
        foreach($profile->getProfileCategories() as $category) {
            $selectedCat[] = $category->getCategory()->getId();
        }

        $form = $this->createForm(ProfileStep1Type::class, $profile);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (count($profile->getProfileCategories()) <= 0) {
                $this->addFlash('danger', 'Veuillez choisir au moins une catégorie.');
            } else if (count($profile->getProfileCategories()) > 5) {
                $this->addFlash('danger', 'Vous ne pouvez pas choisir plus de 5 catégories.');
            } else {
                foreach ($profile->getProfileCategories() as $category) {
                    $category->setProfile($profile);
                }

                // Remove old categories
                foreach ($oldCategories as $c) {
                    if (!$this->searchInCollection($c->getId(), $profile->getProfileCategories())) {
                        $dm->remove($c);
                    }
                }

                $userInfo = $user->getUserInfo();
                $userInfo->setProfile($profile);

                $dm->persist($userInfo);
                $dm->flush();

                return $this->redirectToRoute('candidate_edit_detailed_profile_step2', [
                    'step' => 2,
                ]);
            }
        }

        return $this->render('candidate/editDetailedProfile.html.twig', [
            'form' => $form->createView(),
            'userProfile' => $profile,
            'categories' => $categories,
            'selectedCategories' => $selectedCat,
        ]);
    }

    /**
     * id should start at 2
     *
     * @Route("/candidate/detailed_profile/{step}", name="candidate_edit_detailed_profile_step2", requirements={"step": "\d+"})
     *
     * @param Request $request
     * @param int $step
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editDetailedProfileStep2(Request $request, int $step)
    {
        if ($step === 0 || $step === 1)
            return $this->editDetailedProfileStep1($request);

        $user = $this->getUser();
        $dm = $this->getDoctrine()->getManager();

        // Get all profileCategories of the user to check the step number and get the right category
        $categories = $dm->getRepository('App:ProfileCategory')->getAllByUser($user);
        if (count($categories) + 1 < $step) {
            throw $this->createNotFoundException();
        }
        $category = $categories[$step-2]->getCategory();

        // Get all fields for the given category
        $fieldsRaw = $dm->getRepository('App:Field')->getAllByCategory($category);
        $fields = [];

        // Set categories to use category's id as an array index
        foreach($fieldsRaw as $field) {
            $fields[$field->getId()] = $field;
        }

        $profile = $dm->getRepository('App:Profile')->getByUserAndCategory($user, $category);
        if (!$profile) {
            $profile = $dm->getRepository('App:Profile')->getByUser($user);
            $profile->resetProfileFields();
        }
        $oldFields = clone $profile->getProfileFields();

        $selectedFields = [];
        foreach($profile->getProfileFields() as $field) {
            $selectedFields[] = $field->getField()->getId();
        }

        $form = $this->createForm(ProfileStep2Type::class, $profile);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (count($profile->getProfileFields()) <= 0) {
                $this->addFlash('danger', 'Veuillez choisir au moins une catégorie.');
            } else if (count($profile->getProfileFields()) > 3) {
                $this->addFlash('danger', 'Vous ne pouvez pas choisir plus de 3 catégories.');
            } else {
                foreach ($profile->getProfileFields() as $field) {
                    $field->setProfile($profile);
                }

                // Remove old categories
                foreach ($oldFields as $f) {
                    if (!$this->searchInCollection($f->getId(), $profile->getProfileFields())) {
                        $dm->remove($f);
                    }
                }

                $dm->persist($profile);
                $dm->flush();

                if ($step < 1 + count($categories)) {
                    return $this->redirectToRoute('candidate_edit_detailed_profile_step2', [
                        'step' => $step + 1,
                    ]);
                } else {

                    $this->addFlash('success', 'Profil détaillé mis à jour.');

                    return $this->redirectToRoute('candidate_edit_profile');
                }
            }
        }

        return $this->render('candidate/editDetailedProfileStep2.html.twig', [
            'form' => $form->createView(),
            'userProfile' => $profile,
            'fields' => $fields,
            'selectedFields' => $selectedFields,
            'category' => $category,
            'stepCount' => 1+count($categories),
            'currentStep' => $step,
            'lastStep' => $step === 1+count($categories),
        ]);
    }

    /**
     * @param int $id
     * @param Collection $cat
     * @return bool
     */
    private function searchInCollection(int $id, Collection $cat) {
        foreach ($cat as $c) {
            if ($c->getId() === $id) return true;
        }
        return false;
    }
}
