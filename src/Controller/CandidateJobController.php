<?php

namespace App\Controller;

use App\Entity\Application;
use App\Entity\Job;
use App\Entity\Society;
use App\Form\ApplicationType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CandidateJobController extends Controller
{
    /**
     * @Route("/candidate/jobs/{page}", name="candidate_browse_jobs", requirements={"page": "\d+"}, defaults={"page": 1})
     *
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function browseJobs(int $page) {
        $user = $this->getUser();
        if (!$user->isValid()) {
            $this->addFlash('warning', 'Veuillez compléter votre profil avant de consulter les offres.');
            return $this->redirectToRoute('candidate_edit_profile');
        }

        $jobRepo = $this->getDoctrine()->getManager()->getRepository('App:Job');

        $jobsPager = $jobRepo->search();

        $jobsPager->setMaxPerPage(5);
        $jobsPager->setCurrentPage($page);


        return $this->render('candidate_job/browseJobs.html.twig', [
            'jobsPager' => $jobsPager,
            'shortTypes' => Job::SHORT_TYPE,
        ]);
    }

    /**
     * @Route("/candidate/job/{id}", name="candidate_show_job", requirements={"id": "\d+"})
     *
     * @param int $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showJob(Request $request, int $id) {
        $em = $this->getDoctrine()->getManager();
        $jobRepo = $em->getRepository('App:Job');

        $job = $jobRepo->get($id);

        if ($job === null) throw $this->createNotFoundException();

        $application = new Application();
        $application->setUserInfo($this->getUser()->getUserInfo());
        $application->setJob($job);

        $applicationForm = $this->createForm(ApplicationType::class, $application);
        $applicationForm->handleRequest($request);

        if ($applicationForm->isSubmitted() && $applicationForm->isValid()) {
            $em->persist($application);
            $em->flush();

            $this->addFlash('success', 'Candidature envoyée.');
            return $this->redirectToRoute('candidate_show_job', [
                'id' => $job->getId(),
            ]);
        }

        return $this->render('candidate_job/showJob.html.twig', [
            'job' => $job,
            'society' => $job->getSociety(),
            'applicationForm' => $applicationForm->createView(),
            'sectors' => Society::SECTORS,
        ]);
    }

    /**
     * @Route("/candidate/applications/{page}", name="candidate_show_applications", requirements={"page": "\d+"}, defaults={"page": 1})
     *
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showApplications(int $page)
    {
        $applicationRepo = $this->getDoctrine()->getManager()->getRepository('App:Application');

        $appPager = $applicationRepo->getAllByUser($this->getUser());

        $appPager->setMaxPerPage(5);
        $appPager->setCurrentPage($page);

        return $this->render('candidate_job/showApplications.html.twig', [
            'appPager' => $appPager,
        ]);
    }
}
