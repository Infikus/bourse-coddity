<?php

namespace App\Controller;

use App\Entity\Application;
use App\Entity\Job;
use App\Entity\Profile;
use App\Entity\UserInfo;
use App\Form\JobType;
use App\Form\ProfileStep1Type;
use App\Form\ProfileStep2Type;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SocietyJobController extends Controller
{
    /**
     * @Route("/society/new_job", name="society_post_job")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function postJob(Request $request)
    {
        $user = $this->getUser();

        $job = new Job();
        $job->setSociety($user->getSociety());

        return $this->jobForm($request, $job, true);
    }


    /**
     * @Route("/society/job/{id}", name="society_edit_job", requirements={"id": "\d+"})
     *
     * @param Request $request
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editJob(Request $request, int $id)
    {
        $jobRepo = $this->getDoctrine()->getManager()->getRepository('App:Job');
        $job = $jobRepo->find($id);
        if ($job === null) {
            throw $this->createNotFoundException("Offre non trouvée.");
        }

        return $this->jobForm($request, $job, false);
    }

    private function jobForm(Request $request, Job $job, bool $new)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(JobType::class, $job);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($job);
            $em->flush();

            if ($new)
                $this->addFlash('success', 'Offre crée.');
            else
                $this->addFlash('success', 'Offre mise à jour.');

            return $this->redirectToRoute('society_edit_best_profile', [
                'id' => $job->getId(),
            ]);
        }

        return $this->render('society_job/postJob.html.twig', [
            'form' => $form->createView(),
            'new' => $new,
        ]);
    }

    /**
     * @Route("/society/jobs/{page}", name="society_show_jobs", requirements={"page": "\d+"}, defaults={"page": 1})
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(int $page)
    {
        $user = $this->getUser();
        if (!$user->isValid()) {
            $this->addFlash('warning', 'Veuillez compléter le profil de votre entreprise avant de continuer.');
            return $this->redirectToRoute('society_edit_profile');
        }

        $jobRepo = $this->getDoctrine()->getManager()->getRepository('App:Job');

        $mySociety = $this->getUser()->getSociety();
        $jobsPager = $jobRepo->getSocietyJobs($mySociety);

        $jobsPager->setMaxPerPage(5);
        $jobsPager->setCurrentPage($page);

        return $this->render('society_job/index.html.twig', [
            'jobsPager' => $jobsPager,
            'shortTypes' => Job::SHORT_TYPE,
        ]);
    }

    /**
     * @Route("/society/applications/{page}", name="society_show_applications", requirements={"page": "\d+"}, defaults={"page": 1})
     *
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showApplications(int $page)
    {
        $applicationRepo = $this->getDoctrine()->getManager()->getRepository('App:Application');

        $mySociety = $this->getUser()->getSociety();
        $appPager = $applicationRepo->getAllBySociety($mySociety);

        $appPager->setMaxPerPage(5);
        $appPager->setCurrentPage($page);

        return $this->render('society_job/showApplications.html.twig', [
            'appPager' => $appPager,
        ]);
    }

    /**
     * @Route("/society/application/{id}", name="society_show_application", requirements={"id": "\d+"})
     */
    public function showApplication(int $id)
    {
        $applicationRepo = $this->getDoctrine()->getManager()->getRepository('App:Application');

        $mySociety = $this->getUser()->getSociety();
        $application = $applicationRepo->get($id);

        if ($application === null || $application->getJob()->getSociety()->getId() !== $mySociety->getId()) {
            throw $this->createNotFoundException();
        }

        return $this->render('society_job/showApplication.html.twig', [
            'application' => $application,
            'userStatus' => UserInfo::STATUS,
        ]);
    }

    /**
     * @Route("/society/application/{id}/{action}",
     *     name="society_set_application_status",
     *     requirements={
     *         "id": "\d+",
     *         "action": "(accept|reject)"
     *     })
     *
     * @param int $id
     * @param string $action
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function setApplicationStatus(int $id, string $action)
    {
        $em = $this->getDoctrine()->getManager();
        $applicationRepo = $em->getRepository('App:Application');

        $mySociety = $this->getUser()->getSociety();
        $application = $applicationRepo->get($id);

        if ($application === null || $application->getJob()->getSociety()->getId() !== $mySociety->getId()) {
            return $this->createNotFoundException();
        }

        if ($action === 'accept') {
            $application->setStatus(Application::STATUS_ACCEPTED);
        } else {
            $application->setStatus(Application::STATUS_REJECTED);
        }

        $em->persist($application);
        $em->flush();

        $this->addFlash('success', 'Status de la canditure modifié.');

        return $this->redirectToRoute('society_show_application', [
            'id' => $id,
        ]);
    }

    /**
     * @Route("/society/job/{id}/best_profile", name="society_edit_best_profile", requirements={"id": "\d+"})
     *
     * @param Request $request
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editDetailedProfileStep1(Request $request, int $id)
    {
        $user = $this->getUser();
        $dm = $this->getDoctrine()->getManager();

        // Check the job id
        $job = $dm->getRepository('App:Job')->get($id);
        if (!$job) {
            throw $this->createNotFoundException();
        }

        // Get all categories
        $categoriesRaw = $dm->getRepository('App:Category')->getAll();
        $categories = [];

        // Set categories to use category's id as an array index
        foreach ($categoriesRaw as $category) {
            $categories[$category->getId()] = $category;
        }

        $profile = $dm->getRepository('App:Profile')->getByJob($job) ?: new Profile();
        $profile->setJob($job);
        $oldCategories = clone $profile->getProfileCategories();

        $selectedCat = [];
        foreach ($profile->getProfileCategories() as $category) {
            $selectedCat[] = $category->getCategory()->getId();
        }

        $form = $this->createForm(ProfileStep1Type::class, $profile);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (count($profile->getProfileCategories()) <= 0) {
                $this->addFlash('danger', 'Veuillez choisir au moins une catégorie.');
            } else if (count($profile->getProfileCategories()) > 5) {
                $this->addFlash('danger', 'Vous ne pouvez pas choisir plus de 5 catégories.');
            } else {
                foreach ($profile->getProfileCategories() as $category) {
                    $category->setProfile($profile);
                }

                // Remove old categories
                foreach ($oldCategories as $c) {
                    if (!$this->searchInCollection($c->getId(), $profile->getProfileCategories())) {
                        $dm->remove($c);
                    }
                }

                $job->setBestProfile($profile);

                $dm->persist($job);
                $dm->flush();

                return $this->redirectToRoute('society_edit_best_profile_step2', [
                    'step' => 2,
                    'id' => $id,
                ]);
            }
        }

        return $this->render('society_job/editBestProfile.html.twig', [
            'form' => $form->createView(),
            'userProfile' => $profile,
            'categories' => $categories,
            'selectedCategories' => $selectedCat,
        ]);
    }

    /**
     * id should start at 2
     *
     * @Route("/society/job/{id}/best_profile/{step}", name="society_edit_best_profile_step2", requirements={"step": "\d+", "id": "\d+"})
     *
     * @param Request $request
     * @param int $step
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editDetailedProfileStep2(Request $request, int $step, int $id)
    {
        if ($step === 0 || $step === 1)
            return $this->editDetailedProfileStep1($request, $id);

        $dm = $this->getDoctrine()->getManager();

        // Check the job id
        $job = $dm->getRepository('App:Job')->get($id);

        if (!$job) {
            throw $this->createNotFoundException();
        }

        $user = $this->getUser();

        // Get all profileCategories of the user to check the step number and get the right category
        $categories = $dm->getRepository('App:ProfileCategory')->getAllByJob($job);
        if (count($categories) + 1 < $step) {
            throw $this->createNotFoundException();
        }
        $category = $categories[$step - 2]->getCategory();

        // Get all fields for the given category
        $fieldsRaw = $dm->getRepository('App:Field')->getAllByCategory($category);
        $fields = [];

        // Set categories to use category's id as an array index
        foreach ($fieldsRaw as $field) {
            $fields[$field->getId()] = $field;
        }

	$profile = $dm->getRepository('App:Profile')->getByJobAndCategory($job, $category);
        if (!$profile) {
            $profile = $dm->getRepository('App:Profile')->getByJob($job);
            $profile->resetProfileFields();
	}

	foreach ($profile->getProfileFields() as $field) {
		if ($field->getField()->getCategory()->getId() !== $category->getId()) {
			$profile->removeProfileField($field);
		}
	}

        $oldFields = clone $profile->getProfileFields();

        $selectedFields = [];
        foreach ($profile->getProfileFields() as $field) {
            $selectedFields[] = $field->getField()->getId();
        }

        $form = $this->createForm(ProfileStep2Type::class, $profile);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (count($profile->getProfileFields()) <= 0) {
                $this->addFlash('danger', 'Veuillez choisir au moins une catégorie.');
            } else if (count($profile->getProfileFields()) > 3) {
                $this->addFlash('danger', 'Vous ne pouvez pas choisir plus de 3 catégories.');
            } else {
                foreach ($profile->getProfileFields() as $field) {
                    $field->setProfile($profile);
                }

                // Remove old categories
                foreach ($oldFields as $f) {
                    if (!$this->searchInCollection($f->getId(), $profile->getProfileFields())) {
                        $dm->remove($f);
                    }
                }

                $dm->persist($profile);
                $dm->flush();

                if ($step < 1 + count($categories)) {
                    return $this->redirectToRoute('society_edit_best_profile_step2', [
                        'step' => $step + 1,
                        'id' => $id,
                    ]);
                } else {
                    $this->addFlash('success', 'Profil idéal pour l\'offre "' . $job->getTitle() . '" défini.');

                    return $this->redirectToRoute('society_show_jobs');
                }
            }
        }

        return $this->render('society_job/editBestProfileStep2.html.twig', [
            'form' => $form->createView(),
            'userProfile' => $profile,
            'fields' => $fields,
            'selectedFields' => $selectedFields,
            'category' => $category,
            'stepCount' => 1 + count($categories),
            'currentStep' => $step,
            'lastStep' => $step === 1 + count($categories),
            'id' => $id,
        ]);
    }

    /**
     * @param int $id
     * @param $col
     * @return bool
     */
    private function searchInCollection(int $id, $col)
    {
        foreach ($col as $c) {
            if ($c->getId() === $id) return true;
        }
        return false;
    }

    /**
     * @Route("/society/job/{id}/candidates/{page}",
     *     name="society_browse_candidates",
     *     requirements={"id": "\d+", "page": "\d+"},
     *     defaults={"page": 1}
     *     )
     *
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function findCandidates(int $id)
    {
        $dm = $this->getDoctrine()->getManager();

        // Check job id
        $job = $dm->getRepository('App:Job')->get($id);
        if ($job === null) {
            throw $this->createNotFoundException();
        }

        // Check if there is a best profile for this job
        if (!$bestProfile = $job->getBestProfile()) {
            throw $this->createNotFoundException('Il doit y avoir un profil idéal');
        }

        // Get all candidates with a profile
        $candidates = $dm->getRepository('App:UserInfo')->getAllWithProfile();
        $bestProfileDetails = $bestProfile->getDetailsArray();
        $scores = [];

        foreach($candidates as $candidate)
        {
            $scores[$candidate->getId()] = Profile::calculMatchingScore($bestProfileDetails, $candidate->getProfile()->getDetailsArray());
        }

        usort($candidates, $this->sortCandidate($scores));

        return $this->render('society_job/browseProfiles.html.twig', [
            'candidates' => $candidates,
            'scores' => $scores,
        ]);
    }

    private function sortCandidate($scores)
    {
        return function ($c1, $c2) use ($scores) {
            return -$scores[$c1->getId()] + $scores[$c2->getId()];
        };
    }

    /**
     * @Route("/society/show_profile/{id}", name="society_show_profile", requirements={"id": "\d+"})
     *
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showProfile(int $id) {
        $profile = $this->getDoctrine()->getManager()->getRepository('App:Profile')->find($id);
        if (!$profile) {
            throw $this->createNotFoundException();
        }

        return $this->render('society_job/showProfile.html.twig', [
            'profile' => $profile,
            'userStatus' => UserInfo::STATUS,
        ]);
    }
}
