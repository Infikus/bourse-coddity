<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class IndexController extends Controller
{
    /**
     * @Route("/", name="app_index")
     */
    public function index()
    {
        return $this->render('index/index.html.twig', [
            'controller_name' => 'IndexController',
        ]);
    }

    /**
     * When user are connected
     * Redirect to /candidate or /society
     *
     * @Route("/home", name="app_home")
     */
    public function home()
    {
        $user = $this->getUser();
        if ($user->isCandidate()) {
            return $this->redirectToRoute("candidate_browse_jobs");
        } else {
            return $this->redirectToRoute("society_show_jobs");
        }
    }
}
