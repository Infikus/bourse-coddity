<?php

namespace App\Controller;

use App\Entity\Society;
use App\Form\SocietyType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SocietyController extends Controller
{
    /**
     * @Route("/society/profile", name="society_edit_profile")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editProfile(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $societyRepo = $em->getRepository('App:Society');
        $user = $this->getUser();

        $society = $societyRepo->getByUser($user);
        $society = $society ? $society : new Society(); // Create a new society if the user has not already created one

        $form = $this->createForm(SocietyType::class, $society);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if (!$society->getUser()) {
                $society->setUser($user);
            }

            $em->persist($society);
            $em->flush();

            $this->addFlash('success', 'Entreprise mise à jour.');

            return $this->redirectToRoute('society_edit_profile');
        }

        return $this->render('society/editProfile.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
